const appendSeconds = document.getElementById("seconds");
const appendTens = document.getElementById("tens");

const start = document.getElementById("button-start");
const stop = document.getElementById("button-stop");
const reset = document.getElementById("button-reset");

let tens = 00;
let seconds = 00;
let Interval;
start.addEventListener("click", () => {
    Interval = setInterval(startTimer, 10);
})

function startTimer() {
    tens++
    if (tens <= 9)
        appendTens.innerText = "0" + tens;
    if (tens > 9)
        appendTens.innerText = tens;
    if (tens > 99) {
        seconds++;
        appendSeconds.innerHTML = "0" + seconds;
        tens = 0;
        appendTens.innerHTML = "0" + 0;
    }
    if(seconds>9){
        appendSeconds.innerHTML =  seconds; 
    }
}

stop.addEventListener("click", () => {
   clearInterval(Interval)
})

reset.addEventListener("click", () => {
    tens= "00";
    seconds = "00";
    appendTens.innerText = tens;
    appendSeconds.innerHTML = seconds;
    clearInterval(Interval); 
 })